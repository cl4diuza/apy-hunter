const hre = require("hardhat");
const { URLSearchParams } = require('url');

async function main() {
  console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // Init Contract Token
  const Beefy = await hre.ethers.getContractFactory("BeefyPriceMulticall");

  // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const beefy = await Beefy.deploy();
  console.log("beefy: ", beefy.address);
  console.log("------------- Deployed  ---------------");

  await Promise.all([
    beefy.deployed(),
  ]);

  // Verify Subscription Contract
  await hre.run(
    "verify:verify", {
      address: beefy.address
  }
  );
  console.log('Deployment done.');
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
