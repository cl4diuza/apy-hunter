const {hre, upgrades, ethers} = require("hardhat");
const { URLSearchParams } = require('url');

const TOKEN = "0x5515daB7b4579666e8e3F64fBEeD8A9790A0D532";
const MASTERCHEF = "0x90dbbd2b7e477dB6375C0f7AbB88f66F0bda412c";
const ADMIN = "0x9be29dD2bcA0a42F738D9478295bd0e2dAff433F";
const TREASURY = "0x9be29dD2bcA0a42F738D9478295bd0e2dAff433F";

const MAX_PERFORMANCE_FEE = 500; // 5%
const MAX_CALL_FEE = 100; // 1%
const MAX_WITHDRAW_FEE = 100; // 1%
const MAX_WITHDRAW_FEE_PERIOD = 86400; // 3 days

const performanceFee = 200; // 2%
const callFee = 25; // 0.25%
const withdrawFee = 10; // 0.1%
const withdrawFeePeriod = 86400; // 3 days

async function main() {
  console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // Init Contract Token
  const MooseVault = await ethers.getContractFactory("MooseVault");

  // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const mooseVault = await upgrades.deployProxy(MooseVault, [TOKEN, MASTERCHEF, ADMIN, TREASURY], { initializer: 'initialize'});
  console.log("mooseVault: ", mooseVault.address);
  console.log("------------- Deployed  ---------------");
  await mooseVault.deployed();
  
  const Vault = await ethers.getContractFactory("MooseVault");
  const vault = await Vault.attach(mooseVault.address);
  console.log('get contract');
  await vault.setConfig(
    MAX_PERFORMANCE_FEE, MAX_CALL_FEE, MAX_WITHDRAW_FEE ,MAX_WITHDRAW_FEE_PERIOD
    );
  await vault.setMoreConfig(
    performanceFee, callFee, withdrawFee, withdrawFeePeriod
    );
  console.log('setup done');
}

main()
    .then(() => process.exit(0))
    .catch(error => {
      console.error(error);
      process.exit(1);
    });
