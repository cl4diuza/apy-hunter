const hre = require("hardhat");
const { URLSearchParams } = require('url');

const MOOSE = "0xB37584c1896de835757C3BF20f5f7077c4fB335F";
const START_BLOCK = "14535909";

async function main() {
  console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // Init Contract Token
  const MooseHunt = await hre.ethers.getContractFactory("MooseHunt");

  // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const mooseHunt = await MooseHunt.deploy(MOOSE, START_BLOCK);
  console.log("mooseHunt: ", mooseHunt.address);
  console.log("------------- Deployed  ---------------");

  await Promise.all([
    mooseHunt.deployed(),
  ]);

  // Verify Subscription Contract
  await hre.run(
    "verify:verify", {
      address: mooseHunt.address,
      constructorArguments: [
        MOOSE,
        START_BLOCK
      ]
  }
  );
  console.log('Deployment done.');
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
