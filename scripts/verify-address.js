const { ethers, upgrades } = require('hardhat');
const { URLSearchParams } = require('url');
async function main() {
  
  global.URLSearchParams = URLSearchParams;
  
  await hre.run(
    "verify:verify", {
      address: "0xE22Ec6d2BB34FA125B824ff8571C6f34f8C4505e"
  }
  );
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
