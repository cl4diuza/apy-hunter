const hre = require("hardhat");
const { URLSearchParams } = require('url');

const OWNER = "0x9be29dD2bcA0a42F738D9478295bd0e2dAff433F";

async function main() {


  console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // Init Contract Token
  const Earn = await hre.ethers.getContractFactory("Earn");

  // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const earn = await Earn.deploy(OWNER);
  console.log("earn: ", earn.address);
  console.log("------------- Deployed  ---------------");

  await Promise.all([
    earn.deployed(),
  ]);

  // Verify Subscription Contract
  await hre.run(
    "verify:verify", {
      address: earn.address,
      constructorArguments: [
        OWNER
      ]
  }
  );
  console.log('Deployment done.');
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
