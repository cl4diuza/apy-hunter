const {hre, upgrades, ethers} = require("hardhat");
const { URLSearchParams } = require('url');

const OWNER = "0x9be29dD2bcA0a42F738D9478295bd0e2dAff433F";

async function main() {
  console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // Init Contract Token
  const Earn = await ethers.getContractFactory("Earn");

  // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const earn = await upgrades.deployProxy(Earn, [OWNER], {initializer: 'initialize'});
  console.log("earn: ", earn.address);
  console.log("------------- Deployed  ---------------");
  console.log('Deployment done.');
  await earn.deployed();
  console.log(`deployer address: ${earn.deployTransaction.from}`);
  console.log(`gas price: ${earn.deployTransaction.gasPrice}`);
  console.log(`gas used: ${earn.deployTransaction.gasLimit}`);
}

main()
    .then(() => process.exit(0))
    .catch(error => {
      console.error(error);
      process.exit(1);
    });
