const hre = require("hardhat");
const { URLSearchParams } = require('url');


const AUTO_MOOSE = "0x3C4B28e40965154C7FD34f2538beFccF5ECB89aD";
const STAKED_TOKEN = "0x5081F7d88Cba44e88225Cb177c41e16c1635e22A";
const STAKED_TOKEN_FARM = "0x944f753B2c9B1982454A0e1C4c7FBA6a3ecDA380";
const FARM_REWARD_TOKEN = "0x5081F7d88Cba44e88225Cb177c41e16c1635e22A";
const FARM_PID = 0;
const ISCAKESTAKING = true;
const ROUTER = "0x9Ac64Cc6e4415144C455BD8E4837Fea55603e5c3";
const PATHTOMOOSE1 = "0x5081F7d88Cba44e88225Cb177c41e16c1635e22A";
const PATHTOMOOSE2 = "0xB37584c1896de835757C3BF20f5f7077c4fB335F";
const PATHTOWBNB1 = "0x5081F7d88Cba44e88225Cb177c41e16c1635e22A";
const PATHTOWBNB2 = "0xae13d989daC2f0dEbFf460aC112a837C89BAa7cd";

// const TREASURY = "0x9be29dD2bcA0a42F738D9478295bd0e2dAff433F";
// const KEEPER = "0x9140f75c83352F09dd8743a6262450416785fFe7";
// const PLATFORM = "0x9be29dD2bcA0a42F738D9478295bd0e2dAff433F";
// const BUYBACKRATE = 0;
// const PLATFORMFEE = 300;
const pathMoose = "[0x5081F7d88Cba44e88225Cb177c41e16c1635e22A,0xB37584c1896de835757C3BF20f5f7077c4fB335F]";
/*
  0x5081F7d88Cba44e88225Cb177c41e16c1635e22A
  0x78867bbeef44f2326bf8ddd1941a4439382ef2a7
  0xae13d989daC2f0dEbFf460aC112a837C89BAa7cd
  0xB37584c1896de835757C3BF20f5f7077c4fB335F
*/
const pathWBNB = "[0x5081F7d88Cba44e88225Cb177c41e16c1635e22A,0xae13d989daC2f0dEbFf460aC112a837C89BAa7cd]";

async function main() {
  const PATHTOMOOSE = [PATHTOMOOSE1, PATHTOMOOSE2];
  const PATHTOWBNB = [PATHTOWBNB1, PATHTOWBNB2];


  console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // Init Contract Token
  const HunterVault = await hre.ethers.getContractFactory("HunterVault", "gasLimit: 2100000");

  // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const hunterVault = await HunterVault.deploy(
    AUTO_MOOSE, STAKED_TOKEN, STAKED_TOKEN_FARM, FARM_REWARD_TOKEN,
    FARM_PID, ISCAKESTAKING, ROUTER
    );  
  console.log("hunterVault: ", hunterVault.address);
  console.log("------------- Deployed  ---------------");

  await Promise.all([
    hunterVault.deployed(),
  ]);

  // Verify Subscription Contract
  await hre.run(
    "verify:verify", {
      address: hunterVault.address,
      constructorArguments: [
        AUTO_MOOSE, STAKED_TOKEN, STAKED_TOKEN_FARM, FARM_REWARD_TOKEN,
        FARM_PID, ISCAKESTAKING, ROUTER
      ]
  }
  );
  console.log('Deployment done.');
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
