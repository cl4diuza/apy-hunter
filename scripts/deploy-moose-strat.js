const hre = require("hardhat");
const { URLSearchParams } = require('url');

const MOOSE_TOKEN = "0xB37584c1896de835757C3BF20f5f7077c4fB335F";
const MOOSE_MASTERCHEF = "0x1dD63e7018362012F33425d775317EC0Bbcd5439";
const GOV_ADDRESS = "0x9be29dD2bcA0a42F738D9478295bd0e2dAff433F";

async function main() {
  console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // Init Contract Token
  const StratMoose = await hre.ethers.getContractFactory("StratMoose");

  // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const stratMoose = await StratMoose.deploy(MOOSE_TOKEN, MOOSE_MASTERCHEF, GOV_ADDRESS);
  console.log("stratMoose: ", stratMoose.address);
  console.log("------------- Deployed  ---------------");

  await Promise.all([
    stratMoose.deployed(),
  ]);

  // Verify Subscription Contract
  await hre.run(
    "verify:verify", {
      address: stratMoose.address,
      constructorArguments: [
        MOOSE_TOKEN,
        MOOSE_MASTERCHEF,
        GOV_ADDRESS
      ]
  }
  );
  // console.log('Deployment done.');
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
