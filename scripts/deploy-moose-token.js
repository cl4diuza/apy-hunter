const hre = require("hardhat");
const { URLSearchParams } = require('url');

async function main() {
  console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // Init Contract Token
  const Moose = await hre.ethers.getContractFactory("Moose");

  // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const moose = await Moose.deploy();
  console.log("moose: ", moose.address);
  console.log("------------- Deployed  ---------------");

  await Promise.all([
    moose.deployed(),
  ]);

  // Verify Subscription Contract
  await hre.run(
    "verify:verify", {
      address: moose.address
  }
  );
  console.log('Deployment done.');
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
