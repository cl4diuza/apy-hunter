const {hre, upgrades, ethers} = require("hardhat");
const { URLSearchParams } = require('url');

const AUTO_MOOSE = "0x7eC89D6009984b07b1ABE5F3Fb06807ed61e55D4";
const STAKED_TOKEN = "0x5081F7d88Cba44e88225Cb177c41e16c1635e22A";
const STAKED_TOKEN_FARM = "0x944f753B2c9B1982454A0e1C4c7FBA6a3ecDA380";
const FARM_REWARD_TOKEN = " ";
const FARM_PID = 0;
const ISCAKESTAKING = true;
const ROUTER = "0x9Ac64Cc6e4415144C455BD8E4837Fea55603e5c3";

const TREASURY = "0x9be29dD2bcA0a42F738D9478295bd0e2dAff433F";
const KEEPER = "0x6c0b18b58f9aAC5D29e082B9b4c9A7eF84e55254";
const PLATFORM = "0x9be29dD2bcA0a42F738D9478295bd0e2dAff433F";
const BUYBACKRATE = 0;
const PLATFORMFEE = 0;

const pathMoose = ["0x5081F7d88Cba44e88225Cb177c41e16c1635e22A","0x5515daB7b4579666e8e3F64fBEeD8A9790A0D532"];
/*
  0x5081F7d88Cba44e88225Cb177c41e16c1635e22A
  0x78867bbeef44f2326bf8ddd1941a4439382ef2a7
  0xae13d989daC2f0dEbFf460aC112a837C89BAa7cd
  0xB37584c1896de835757C3BF20f5f7077c4fB335F
*/
const pathWBNB = ["0x5081F7d88Cba44e88225Cb177c41e16c1635e22A","0xae13d989daC2f0dEbFf460aC112a837C89BAa7cd"];

const WBNB = "0xae13d989daC2f0dEbFf460aC112a837C89BAa7cd";
const MOOSE = "0x5515daB7b4579666e8e3F64fBEeD8A9790A0D532";

const keeperFee = 50; // 0.5%
const keeperFeeUL = 100; // 1%
const platformFeeUL = 500; // 5%

const BURN_ADDRESS = "0x000000000000000000000000000000000000dEaD";
const buyBackRateUL = 300; // 5%

const earlyWithdrawFee = 100; // 1%
const earlyWithdrawFeeUL = 300; // 3%
const withdrawFeePeriod = 100;

async function main() {

  console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // Init Contract Token
  const HunterVault = await ethers.getContractFactory("HunterVault");

  // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const hunterVault = await upgrades.deployProxy(HunterVault, 
    [AUTO_MOOSE, STAKED_TOKEN, STAKED_TOKEN_FARM, FARM_REWARD_TOKEN, FARM_PID, ISCAKESTAKING, ROUTER],
    { initializer: 'initialize'});  
  console.log("hunterVault: ", hunterVault.address);
  console.log("------------- Deployed  ---------------");
  await hunterVault.deployed();
  console.log(`deployer address: ${hunterVault.deployTransaction.from}`);
  console.log(`gas price: ${hunterVault.deployTransaction.gasPrice}`);
  console.log(`gas used: ${hunterVault.deployTransaction.gasLimit}`);

  const Vault = await ethers.getContractFactory("HunterVault");
  const vault = await Vault.attach("0xe80a892Bf9537c7Ff114F72e44CFa1148379DF96");
  console.log('get contract');
  await vault['setConfig(address,address,address,uint256,uint256,uint256)'](TREASURY, KEEPER, PLATFORM, BUYBACKRATE, PLATFORMFEE, platformFeeUL);
  await vault['setConfig(address,address,address,uint256,uint256)'](WBNB, MOOSE, BURN_ADDRESS, keeperFee, keeperFeeUL);
  await vault['setConfig(uint256,uint256,uint256,uint256)'](buyBackRateUL, earlyWithdrawFee, earlyWithdrawFeeUL, withdrawFeePeriod);
  await vault.setPathToMoose(pathMoose);
  await vault.setPathToWbnb(pathWBNB);
  console.log('setup done');
}

main()
    .then(() => process.exit(0))
    .catch(error => {
      console.error(error);
      process.exit(1);
    });

