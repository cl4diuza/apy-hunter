const hre = require("hardhat");
const { URLSearchParams } = require('url');

const MOOSE_TOKEN = "0x5515daB7b4579666e8e3F64fBEeD8A9790A0D532";
const MOOSE_MASTERCHEF = "0x90dbbd2b7e477dB6375C0f7AbB88f66F0bda412c";
const GOV_ADDRESS = "0x9be29dD2bcA0a42F738D9478295bd0e2dAff433F";


const entranceFeeFactor = 10000; // No deposit fees
const entranceFeeFactorMax = 10000;
const entranceFeeFactorLL = 9950; // 0.5% is the max entrance fee settable. LL = lowerlimit

const withdrawFeeFactor = 10000; // No withdraw fees
const withdrawFeeFactorMax = 10000;
const withdrawFeeFactorLL = 9950; // 0.5% is the max entrance fee settable. LL = lowerlimit

async function main() {
  console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // Init Contract Token
  const StratMoose = await hre.ethers.getContractFactory("StratMoose");

  // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const stratMoose = await upgrades.deployProxy(StratMoose, [MOOSE_TOKEN, MOOSE_MASTERCHEF, GOV_ADDRESS], {initializer: 'initialize'});
  console.log("stratMoose: ", stratMoose.address);
  console.log("------------- Deployed  ---------------");
  await stratMoose.deployed();
  console.log(`deployer address: ${stratMoose.deployTransaction.from}`);
  console.log(`gas price: ${stratMoose.deployTransaction.gasPrice}`);
  console.log(`gas used: ${stratMoose.deployTransaction.gasLimit}`);

  const Strat = await ethers.getContractFactory("StratMoose");
  const strat = await Strat.attach(stratMoose.address);
  console.log('get contract');
  await strat.setConfig(
    entranceFeeFactor, entranceFeeFactorMax, entranceFeeFactorLL,
    withdrawFeeFactor, withdrawFeeFactorMax, withdrawFeeFactorLL
    );
  console.log('setup done');

}

main()
    .then(() => process.exit(0))
    .catch(error => {
      console.error(error);
      process.exit(1);
    });
