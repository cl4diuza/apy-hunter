const {hre, upgrades, ethers} = require("hardhat");
const { URLSearchParams } = require('url');

const MOOSE = "0x5515daB7b4579666e8e3F64fBEeD8A9790A0D532";
const BURN_ADDRESS = "0x000000000000000000000000000000000000dEaD";

const OWNER_MOOSE_REWARD = "200";
const MAX_SUPPLY = "100000000000000000000000000";
const MOOSE_PER_BLOCK = "2000000000000000000";
const START_BLOCK = "14813648";
const ALLOCATION_POINT = "0"

async function main() {
  console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // Init Contract Token
  const MooseHunt = await ethers.getContractFactory("MooseHunt");

  // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const mooseHunt = await upgrades.deployProxy(MooseHunt
    ,
     [MOOSE, BURN_ADDRESS, OWNER_MOOSE_REWARD, START_BLOCK],
     { initializer: 'initialize'},
     {gasLimit: 2147443, gasPrice: 10000000000}
     );
  console.log("mooseHunt: ", mooseHunt.address);
  console.log("------------- Deployed  ---------------");
  await mooseHunt.deployed();
  console.log(`deployer address: ${mooseHunt.deployTransaction.from}`);
  console.log(`gas price: ${mooseHunt.deployTransaction.gasPrice}`);
  console.log(`gas used: ${mooseHunt.deployTransaction.gasLimit}`);

  const Masterchef = await ethers.getContractFactory("MooseHunt");
  const masterchef = await Masterchef.attach(mooseHunt.address);
  console.log('get contract');
  await masterchef.setMaxSupply(MAX_SUPPLY);
  await masterchef.setMoosePerBlock(MOOSE_PER_BLOCK);
  console.log('setup done');
}

main()
    .then(() => process.exit(0))
    .catch(error => {
      console.error(error);
      process.exit(1);
    });
