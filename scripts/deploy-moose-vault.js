const hre = require("hardhat");
const { URLSearchParams } = require('url');

const TOKEN = "0xB37584c1896de835757C3BF20f5f7077c4fB335F";
const MASTERCHEF = "0x1dD63e7018362012F33425d775317EC0Bbcd5439";
const ADMIN = "0x9be29dD2bcA0a42F738D9478295bd0e2dAff433F";
const TREASURY = "0x9be29dD2bcA0a42F738D9478295bd0e2dAff433F";

async function main() {
  console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // Init Contract Token
  const MooseVault = await hre.ethers.getContractFactory("MooseVaultHunter");

  // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const mooseVault = await MooseVault.deploy(TOKEN, MASTERCHEF, ADMIN, TREASURY, 0);
  console.log("mooseVault: ", mooseVault.address);
  console.log("------------- Deployed  ---------------");

  await Promise.all([
    mooseVault.deployed(),
  ]);

  // Verify Subscription Contract
  await hre.run(
    "verify:verify", {
      address: mooseVault.address,
      constructorArguments: [
        TOKEN, MASTERCHEF, ADMIN, TREASURY, 0
      ]
  }
  );
  console.log('Deployment done.');
}

main()
  .then(() => process.exit(0))
  .catch(error => {
    console.error(error);
    process.exit(1);
  });
