const {hre, upgrades, ethers} = require("hardhat");
const { URLSearchParams } = require('url');

const TOKEN = "0x5515daB7b4579666e8e3F64fBEeD8A9790A0D532";
const MASTERCHEF = "0x90dbbd2b7e477dB6375C0f7AbB88f66F0bda412c";
const TREASURY = "0x9be29dD2bcA0a42F738D9478295bd0e2dAff433F";

const MAX_WITHDRAW_FEE = 100; // 1%

const withdrawFee = 10; // 0.1%
const withdrawFeePeriod = 86400; // 3 days

async function main() {
  console.log('deployment start.');
  
  global.URLSearchParams = URLSearchParams;
  // Init Contract Token
  const MooseVault = await ethers.getContractFactory("MooseVaultHunter");

  // Deploy Contract 
  console.log("------------- Start Deployed  ---------------");
  const mooseVault = await upgrades.deployProxy(MooseVault,
    [TOKEN, MASTERCHEF, TREASURY, withdrawFee, withdrawFeePeriod, MAX_WITHDRAW_FEE],
    { initializer: 'initialize'});
  console.log("mooseVault: ", mooseVault.address);
  console.log("------------- Deployed  ---------------");

  await mooseVault.deployed();
  console.log(`deployer address: ${mooseVault.deployTransaction.from}`);
  console.log(`gas price: ${mooseVault.deployTransaction.gasPrice}`);
  console.log(`gas used: ${mooseVault.deployTransaction.gasLimit}`);

  // const Vault = await ethers.getContractFactory("MooseVaultHunter");
  // const vault = await Vault.attach(mooseVault.address);
  // console.log('get contract');
  // await vault.setConfig(TREASURY, KEEPER, PLATFORM, BUYBACKRATE, PLATFORMFEE, platformFeeUL);
  // await vault.setConfig(WBNB, MOOSE, BURN_ADDRESS, keeperFee, keeperFeeUL);
  // await vault.setConfig(buyBackRateUL, earlyWithdrawFee, earlyWithdrawFeeUL. withdrawFeePeriod);
  // await vault.setPathToMoose(pathMoose);
  // await vault.setPathToWbnb(pathWBNB);
  // console.log('setup done');
}

main()
    .then(() => process.exit(0))
    .catch(error => {
      console.error(error);
      process.exit(1);
    });


