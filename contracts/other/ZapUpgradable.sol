// SPDX-License-Identifier: MIT

pragma solidity 0.8.10;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";

import "../interfaces/IPancakePair.sol";
import "../interfaces/IPancakeRouter01.sol";

library Babylonian {
    // credit for this implementation goes to
    // https://github.com/abdk-consulting/abdk-libraries-solidity/blob/master/ABDKMath64x64.sol#L687
    function sqrt(uint256 x) internal pure returns (uint256) {
        if (x == 0) return 0;
        // this block is equivalent to r = uint256(1) << (BitMath.mostSignificantBit(x) / 2);
        // however that code costs significantly more gas
        uint256 xx = x;
        uint256 r = 1;
        if (xx >= 0x100000000000000000000000000000000) {
            xx >>= 128;
            r <<= 64;
        }
        if (xx >= 0x10000000000000000) {
            xx >>= 64;
            r <<= 32;
        }
        if (xx >= 0x100000000) {
            xx >>= 32;
            r <<= 16;
        }
        if (xx >= 0x10000) {
            xx >>= 16;
            r <<= 8;
        }
        if (xx >= 0x100) {
            xx >>= 8;
            r <<= 4;
        }
        if (xx >= 0x10) {
            xx >>= 4;
            r <<= 2;
        }
        if (xx >= 0x8) {
            r <<= 1;
        }
        r = (r + x / r) >> 1;
        r = (r + x / r) >> 1;
        r = (r + x / r) >> 1;
        r = (r + x / r) >> 1;
        r = (r + x / r) >> 1;
        r = (r + x / r) >> 1;
        r = (r + x / r) >> 1; // Seven iterations should be enough
        uint256 r1 = x / r;
        return (r < r1 ? r : r1);
    }
}

interface IWBNB {
    function deposit() external payable;
    function withdraw(uint256 wad) external;
}

contract ZapUpgradable is OwnableUpgradeable {
    using SafeMathUpgradeable for uint;
    using SafeERC20Upgradeable for IERC20Upgradeable;

    // Settings
    IPancakeRouter01 public router;
    address public WBNB;
    uint256 public blockDeadLine;
    uint256 public routeCount;

    // pancake route from token x to token y
    mapping(uint256 => address[]) pathRoutes;

    function initialize(address _router, address _WBNB) external {
        router = IPancakeRouter01(_router);
        WBNB = _WBNB;
        blockDeadLine = block.number.add(2000000000);
        routeCount = 0;
        OwnableUpgradeable.__Ownable_init();
    }

    receive() external payable {
        assert(msg.sender == WBNB);
    }

    function giveBNB(uint256 amount) external {
        payable(msg.sender).transfer(amount);
    }

    function unwrapBNB(uint256 amount) external {
        _approveTokenIfNeeded(IERC20Upgradeable(WBNB), amount, address(this));
        _approveTokenIfNeeded(IERC20Upgradeable(WBNB), amount, msg.sender);
        IWBNB(WBNB).withdraw(amount);
    }

    function zapIn(
        address token,
        address pair,
        uint256 amount
    ) external payable returns (uint256 liquidityBought) {
        address token0 = IPancakePair(pair).token0();
        address token1 = IPancakePair(pair).token1();

        // native token
        if (msg.value > 0) {
            (bool sent,) = address(this).call{value: msg.value}("");
            require(sent, "Failed to send BNB");
            IWBNB(WBNB).deposit{value: msg.value}();
            token = WBNB;
            amount = msg.value;
        } else {
            IERC20Upgradeable(token).safeTransferFrom(msg.sender, address(this), amount);
        }

        (uint256 halfAmount, uint256 halfSwapAmount) = _processSwap(token, amount, token0, token1);

        uint256 _amountTokenADesired;
        uint256 _amountTokenBDesired;
        if (token == token0 || (token != token0 && token != token1)) {
            _amountTokenADesired = halfAmount; 
            _amountTokenBDesired = halfSwapAmount;
            _approveTokenIfNeeded(IERC20Upgradeable(token0), halfAmount, address(router));
            _approveTokenIfNeeded(IERC20Upgradeable(token1), halfSwapAmount, address(router));
        } else {
            _amountTokenADesired = halfSwapAmount; 
            _amountTokenBDesired = halfAmount;
            _approveTokenIfNeeded(IERC20Upgradeable(token0), halfSwapAmount, address(router));
            _approveTokenIfNeeded(IERC20Upgradeable(token1), halfAmount, address(router));
        }
        (,,liquidityBought) =  router.addLiquidity(token0,
            token1,
            _amountTokenADesired,
            _amountTokenBDesired,
            0,
            0,
            address(this),
            blockDeadLine);                         
     
        IERC20Upgradeable(pair).safeTransfer(msg.sender, liquidityBought);
    }

    function zapOut(
        address token,
        address pair,
        uint256 liquidity,
        bool isWantPair
    ) external returns (uint256 token0Amount, uint256 token1Amount) {
        // find pair token
        address token0 = IPancakePair(pair).token0();
        address token1 = IPancakePair(pair).token1();

        // get those lp token
        IERC20Upgradeable(pair).safeTransferFrom(msg.sender, address(this), liquidity);

        // approve and remove liquidity
        _approveTokenIfNeeded(IERC20Upgradeable(pair), liquidity, address(router));
        (uint256 amount0, uint256 amount1) =  router.removeLiquidity(token0,
            token1,
            liquidity,
            1,
            1,
            address(this),
            blockDeadLine);

        // want pair, transfer each
        if (isWantPair) {
            IERC20Upgradeable(token0).safeTransfer(msg.sender, amount0);
            if (token1 == WBNB) {
                IWBNB(WBNB).withdraw(amount1);
                payable(msg.sender).transfer(amount1);
            } else {
                IERC20Upgradeable(token1).safeTransfer(msg.sender, amount1);
            }
            token0Amount = amount0;
            token1Amount = amount1;
        } else {
            // want single token, proceed to swap
            uint256[] memory swapAmounts;
            if (token == token0) {
                swapAmounts = _swap(
                    amount1,
                    0,
                    getRoute(token1, token0),
                    address(this)
                );
                // transfer the desire token
                token0Amount = amount0.add(swapAmounts[swapAmounts.length - 1]);
                IERC20Upgradeable(token0).safeTransfer(msg.sender, token0Amount);
                token1Amount = 0;
            } else {
                swapAmounts = _swap(
                    amount0,
                    0,
                    getRoute(token0, token1),
                    address(this)
                );
                token1Amount = amount1.add(swapAmounts[swapAmounts.length - 1]);
                if (token1 == WBNB) {
                    IWBNB(WBNB).withdraw(token1Amount);
                    payable(msg.sender).transfer(token1Amount);
                } else {  
                    // transfer the desire token
                    IERC20Upgradeable(token1).safeTransfer(msg.sender, token1Amount);
                }           
                token0Amount = 0;
            }
        }
    }

    function _processSwap(address _token, uint256 _amount, address token0, address token1) private returns (uint256, uint256) {

        if (_token != token0 && _token != token1) {
            // not token0 or token1, swap each half to complete the pair
            uint256[] memory amountArr0 = _swap(
                _amount.div(2),
                0,
                getRoute(_token, token0),
                address(this)
            );

            uint256[] memory amountArr1 = _swap(
                _amount.div(2),
                0,
                getRoute(_token, token1),
                address(this)
            );
            return (amountArr0[amountArr0.length -1], amountArr1[amountArr1.length -1]);
        } else {
            // swap other half to complete the pair
            address[] memory pathToOther;

            if (_token == token0) {
                pathToOther = getRoute(token0, token1); 
            } else {
                pathToOther = getRoute(token1, token0);
            }
        
            uint256[] memory amounts = _swap(
                _amount.div(2),
                0,
                pathToOther,
                address(this)
            );
            return (_amount.div(2), amounts[amounts.length - 1]);
        }
    }

    function _swap(
        uint256 _inputAmount,
        uint256 _minOutputAmount,
        address[] memory _path,
        address _to
    ) private returns (uint256[] memory) {
        _approveTokenIfNeeded(
            IERC20Upgradeable(_path[0]),
            _inputAmount,
            address(router)
        );
        return router.swapExactTokensForTokens(
            _inputAmount,
            _minOutputAmount,
            _path,
            _to,
            block.timestamp
        );
    }

    function getRoute(address tokenFrom, address tokenTo) public view returns (address[] memory route)  {
        bool isSuccess = false;
        for (uint256 i = 0; i < routeCount; i ++) {
            address[] memory r = pathRoutes[i];
            if (r[0] == tokenFrom && r[r.length - 1] == tokenTo) {      
                isSuccess = true;  
                route = r;
                return route;
            }
        }
        return route;
    }

    function addRoute(uint256 index, address[] calldata route) external onlyOwner {
        bool isSuccess = false;
        if (routeCount == 0) {  
            pathRoutes[index] = route;
            routeCount = routeCount.add(1);
            isSuccess = true;
        } else {
            address[] memory r = pathRoutes[index];
            if (r.length == 0) {
                pathRoutes[index] = route;
                routeCount = routeCount.add(1);
                isSuccess = true;
            }
        }
        require(isSuccess, "Fail");
    }

    function getRoute(uint256 index) external view returns (address[] memory) {
        return pathRoutes[index];
    }

    function setBlockDeadLine(uint256 newDeadLine) external onlyOwner {
        blockDeadLine = newDeadLine;
    }

    function estimateSwap(address pair, address tokenIn, uint256 amount) public view returns(uint256 swapAmountIn, uint256 swapAmountOut, address swapTokenOut) {
        address token0 = IPancakePair(pair).token0();
        address token1 = IPancakePair(pair).token1();

        bool isInputA = token0 == tokenIn;
        require(isInputA || token1 == tokenIn, "Input token not present in liquidity pair");

        (uint256 reserveA, uint256 reserveB,) = IPancakePair(pair).getReserves();
        (reserveA, reserveB) = isInputA ? (reserveA, reserveB) : (reserveB, reserveA);

        uint256 halfInvestment = amount / 2;
        uint256 nominator = router.getAmountOut(halfInvestment, reserveA, reserveB);
        uint256 denominator = router.quote(halfInvestment, reserveA.add(halfInvestment), reserveB.sub(nominator));
        swapAmountIn = amount.sub(Babylonian.sqrt(halfInvestment * halfInvestment * nominator / denominator));
        swapAmountOut = router.getAmountOut(swapAmountIn, reserveA, reserveB);

        swapTokenOut = isInputA ? token1 : token0;
    }

    function _approveTokenIfNeeded(
        IERC20Upgradeable _token,
        uint256 _amount,
        address _spender
    ) private {
        if (_token.allowance(address(this), _spender) < _amount) {
            _token.safeIncreaseAllowance(_spender, _amount);
        }
    }
}