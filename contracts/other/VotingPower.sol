// SPDX-License-Identifier: MIT

pragma solidity 0.8.10;

import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "../interfaces/IPancakePair.sol";

interface IToken {
    function balanceOf(address _user) external view returns (uint256);
}

interface IFarm {
    function stakedWantTokens(
        uint256 _pid,
        address _user
    ) external view returns (uint256);

    function poolLength() external view returns (uint256);

    function poolInfo(uint256 _pid) external view returns (address want);
}

interface IVault {
    function userInfo(
        address _user
    ) external view returns (
        uint256 shares,
        uint256 lastDepositedTime,
        uint256 hunterAtLastUserAction,
        uint256 lastUserActionTime
    );

    function getPricePerFullShare() external view returns (uint256);
}

contract VotingPower is Ownable {
    using SafeMath for uint256;

    IToken public HUNTER = IToken(0x0000000000000000000000000000000000000000);
    IFarm public HUNTER_FARM = IFarm(0x0000000000000000000000000000000000000000);
    IVault public HUNTER_VAULT = IVault(0x0000000000000000000000000000000000000000);

    mapping(uint256 => bool) public hunterPairs;

    event HunterPairAdded(uint256 _pid);
    event HunterPairRemoved(uint256 _pid);

    constructor(address _owner) {
        addHunterPairPid(2);
        addHunterPairPid(15);
        addHunterPairPid(16);
        addHunterPairPid(21);
        addHunterPairPid(22);

        transferOwnership(_owner);
    }

    function votingPower(address _user) external view returns (uint256) {
        uint256 tokenBalance = HUNTER.balanceOf(_user);
        uint256 farmBalance = HUNTER_FARM.stakedWantTokens(0, _user);
        uint256 pairBalance = _getHunterPairBalances(_user);
        uint256 vaultBalance = _getHunterVaultBalance(_user);

        return tokenBalance.add(farmBalance).add(pairBalance).add(vaultBalance);
    }

    function _getHunterVaultBalance(address _user) private view returns (uint256){
        uint256 pricePerShare = HUNTER_VAULT.getPricePerFullShare();
        (uint256 shares, , ,) = HUNTER_VAULT.userInfo(_user);

        return shares.mul(pricePerShare).div(1e18);
    }

    function _getHunterPairBalances(address _user) private view returns (uint256 balance) {
        uint256 length = HUNTER_FARM.poolLength();

        for (uint256 pid = 0; pid < length; ++pid) {
            if (!hunterPairs[pid]) {
                continue;
            }

            uint256 pairBalance = HUNTER_FARM.stakedWantTokens(pid, _user);

            if (pairBalance > 0) {
                balance = balance.add(
                    _getHunterPairBalance(HUNTER_FARM.poolInfo(pid), pairBalance)
                );
            }
        }

        return balance;
    }

    function _getHunterPairBalance(address _pair, uint256 _balance) private view returns (uint256) {
        IPancakePair pair = IPancakePair(_pair);

        bool hunterToken0 = pair.token0() == address(HUNTER);
        bool hunterToken1 = pair.token1() == address(HUNTER);

        if (!hunterToken0 && !hunterToken1) {
            return 0;
        }

        (uint256 reserve0, uint256 reserve1,) = pair.getReserves();

        return hunterToken0
        ? reserve0.mul(_balance).div(pair.totalSupply())
        : reserve1.mul(_balance).div(pair.totalSupply());
    }

    function addHunterPairPid(uint256 _pid) public onlyOwner {
        hunterPairs[_pid] = true;

        emit HunterPairAdded(_pid);
    }

    function removeHunterPairPid(uint256 _pid) public onlyOwner {
        hunterPairs[_pid] = false;

        emit HunterPairRemoved(_pid);
    }
}