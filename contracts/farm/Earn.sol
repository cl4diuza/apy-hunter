// SPDX-License-Identifier: MIT

pragma solidity 0.8.10;

import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";

interface IVault {
    function earn() external;
}

interface IHunterVault {
    function earn(uint256, uint256, uint256, uint256) external;
}

contract Earn is OwnableUpgradeable {

    function initialize(address _owner) external {
        OwnableUpgradeable.__Ownable_init();
        transferOwnership(_owner);
    }

    function earn(
        address[] calldata _legacyVaults
    ) public onlyOwner {
        uint256 legacyLength = _legacyVaults.length;

        for (uint256 index = 0; index < legacyLength; ++index) {
            IVault(_legacyVaults[index]).earn();
        }
    }

    function earnHunter(
        address[] calldata _hunterVaults,
        uint256[] calldata _minPlatformOutputs,
        uint256[] calldata _minKeeperOutputs,
        uint256[] calldata _minBurnOutputs,
        uint256[] calldata _minHunterOutputs
    ) public onlyOwner {
        uint256 sweetLength = _hunterVaults.length;

        for (uint256 index = 0; index < sweetLength; ++index) {
            IHunterVault(_hunterVaults[index]).earn(
                _minPlatformOutputs[index],
                _minKeeperOutputs[index],
                _minBurnOutputs[index],
                _minHunterOutputs[index]
            );
        }

    }

}