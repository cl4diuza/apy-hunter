// SPDX-License-Identifier: MIT

pragma solidity 0.8.10;

import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/ERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";

import "../../interfaces/IPancakeRouter01.sol";
import "../../interfaces/IPancakeswapFarm.sol";
import "../../interfaces/IPancakePair.sol";
import "../../interfaces/IMooseVault.sol";

contract HunterVaultStorage is OwnableUpgradeable, ReentrancyGuardUpgradeable {

    // Addresses
    address public WBNB;
    IERC20Upgradeable public MOOSE;
    IMooseVault public AUTO_MOOSE;
    IERC20Upgradeable public STAKED_TOKEN;

    // Runtime data
    mapping(address => UserInfo) public userInfo; // Info of users

    uint256 public accSharesPerStakedToken; // Accumulated AUTO_MOOSE shares per staked token, times 1e18.

    // Farm info
    IPancakeswapFarm public STAKED_TOKEN_FARM;
    IERC20Upgradeable public FARM_REWARD_TOKEN;
    uint256 public FARM_PID;
    bool IS_CAKE_STAKING;

    // Settings
    IPancakeRouter01 public router;
    address[] public pathToMoose; // Path from staked token to MOOSE
    address[] public pathToWbnb; // Path from staked token to WBNB

    address public treasury;
    address public keeper;
    uint256 public keeperFee;
    uint256 public keeperFeeUL;

    address public platform;
    uint256 public platformFee;
    uint256 public platformFeeUL;

    address public BURN_ADDRESS;
    uint256 public buyBackRate;
    uint256 public buyBackRateUL;

    uint256 public earlyWithdrawFee;
    uint256 public earlyWithdrawFeeUL;
    uint256 public withdrawFeePeriod;

    struct UserInfo {
        // How many assets the user has provided.
        uint256 stake;
        // How many staked $MOOSE user had at his last action
        uint256 autoMooseShares;
        // Moose shares not entitled to the user
        uint256 rewardDebt;
        // Timestamp of last user deposit
        uint256 lastDepositedTime;
    }
}

contract HunterVault is HunterVaultStorage {
    
    using SafeMathUpgradeable for uint256;
    using SafeERC20Upgradeable for IERC20Upgradeable;

    event SetPathToMoose(address[] oldPath, address[] newPath);
    event SetPathToWbnb(address[] oldPath, address[] newPath);
    event SetBuyBackRate(uint256 oldBuyBackRate, uint256 newBuyBackRate);
    event SetTreasury(address oldTreasury, address newTreasury);
    event SetKeeper(address oldKeeper, address newKeeper);
    event SetKeeperFee(uint256 oldKeeperFee, uint256 newKeeperFee);
    event SetPlatform(address oldPlatform, address newPlatform);
    event SetPlatformFee(uint256 oldPlatformFee, uint256 newPlatformFee);
    event SetEarlyWithdrawFee(uint256 oldEarlyWithdrawFee, uint256 newEarlyWithdrawFee);
    
    event Deposit(address indexed user, uint256 amount);
    event Withdraw(address indexed user, uint256 amount);
    event EarlyWithdraw(address indexed user, uint256 amount, uint256 fee);
    event ClaimRewards(address indexed user, uint256 shares, uint256 amount);

    modifier onlyKeeper() {
        require(keeper == msg.sender, "HunterVault: caller is not the keeper");
        _;
    }

    function initialize(
        address _autoMoose,
        address _stakedToken,
        address _stakedTokenFarm,
        address _farmRewardToken,
        uint256 _farmPid,
        bool _isCakeStaking,      
        address _router
    ) external {
        AUTO_MOOSE = IMooseVault(_autoMoose);
        STAKED_TOKEN = IERC20Upgradeable(_stakedToken);
        STAKED_TOKEN_FARM = IPancakeswapFarm(_stakedTokenFarm);
        FARM_REWARD_TOKEN = IERC20Upgradeable(_farmRewardToken);
        FARM_PID = _farmPid;
        IS_CAKE_STAKING = _isCakeStaking;

        router = IPancakeRouter01(_router);
        OwnableUpgradeable.__Ownable_init();
    }

    function setConfig(
        address _treasury,
        address _keeper,
        address _platform,
        uint256 _buyBackRate,
        uint256 _platformFee,
        uint256 _platformFeeUL
    ) external onlyOwner {
        require(_buyBackRate <= buyBackRateUL);
        require(_platformFee <= platformFeeUL);

        buyBackRate = _buyBackRate;
        platformFee = _platformFee;
        platformFeeUL = _platformFeeUL;
        treasury = _treasury;
        keeper = _keeper;
        platform = _platform;
    }

    function setConfig(
        address _WBNB,
        address _MOOSE,
        address _burnAddress,
        uint256 _keeperFee,
        uint256 _keeperFeeUL
    ) external onlyOwner {
        WBNB = _WBNB;
        MOOSE = IERC20Upgradeable(_MOOSE);
        BURN_ADDRESS = _burnAddress;

        keeperFee = _keeperFee;
        keeperFeeUL = _keeperFeeUL;
    }

    function setConfig(
        uint256 _buyBackRateUL,
        uint256 _earlyWithdrawFee,
        uint256 _earlyWithdrawFeeUL,
        uint256 _withdrawFeePeriod
    ) external onlyOwner {
        buyBackRateUL = _buyBackRateUL;
        earlyWithdrawFee = _earlyWithdrawFee;
        earlyWithdrawFeeUL = _earlyWithdrawFeeUL;
        withdrawFeePeriod = _withdrawFeePeriod;
    }

    function earn(
        uint256 _minPlatformOutput,
        uint256 _minKeeperOutput,
        uint256 _minBurnOutput,
        uint256 _minMooseOutput
    ) external onlyKeeper {
        if (IS_CAKE_STAKING) {
            STAKED_TOKEN_FARM.leaveStaking(0);
        } else {
            STAKED_TOKEN_FARM.withdraw(FARM_PID, 0);
        }

        uint256 rewardTokenBalance = FARM_REWARD_TOKEN.balanceOf(address(this));

        // Collect platform fees
        if (platformFee > 0) {
            _swap(
                rewardTokenBalance.mul(platformFee).div(1e4),
                _minPlatformOutput,
                pathToWbnb,
                platform
            );
        }

        // Collect keeper fees
        if (keeperFee > 0) {
            _swap(
                rewardTokenBalance.mul(keeperFee).div(1e4),
                _minKeeperOutput,
                pathToWbnb,
                treasury
            );
        }

        // Collect Burn fees
        if (buyBackRate > 0) {
            _swap(
                rewardTokenBalance.mul(buyBackRate).div(1e4),
                _minBurnOutput,
                pathToMoose,
                BURN_ADDRESS
            );
        }

        // Convert remaining rewards to MOOSE
        _swap(
            FARM_REWARD_TOKEN.balanceOf(address(this)),
            _minMooseOutput,
            pathToMoose,
            address(this)
        );

        uint256 previousShares = totalAutoMooseShares();
        uint256 mooseBalance = MOOSE.balanceOf(address(this));

        _approveTokenIfNeeded(
            MOOSE,
            mooseBalance,
            address(AUTO_MOOSE)
        );

        AUTO_MOOSE.deposit(mooseBalance);

        uint256 currentShares = totalAutoMooseShares();

        accSharesPerStakedToken = accSharesPerStakedToken.add(
            currentShares.sub(previousShares).mul(1e18).div(totalStake())
        );
    }

    function deposit(uint256 _amount) public nonReentrant {
        require(_amount > 0, "HunterVault: amount must be greater than zero");

        UserInfo storage user = userInfo[msg.sender];

        STAKED_TOKEN.safeTransferFrom(
            address(msg.sender),
            address(this),
            _amount
        );

        _approveTokenIfNeeded(
            STAKED_TOKEN,
            _amount,
            address(STAKED_TOKEN_FARM)
        );

        if (IS_CAKE_STAKING) {
            STAKED_TOKEN_FARM.enterStaking(_amount);
        } else {
            STAKED_TOKEN_FARM.deposit(FARM_PID, _amount);
        }

        user.autoMooseShares = user.autoMooseShares.add(
            user.stake.mul(accSharesPerStakedToken).div(1e18).sub(
                user.rewardDebt
            )
        );
        user.stake = user.stake.add(_amount);
        user.rewardDebt = user.stake.mul(accSharesPerStakedToken).div(1e18);
        user.lastDepositedTime = block.timestamp;

        emit Deposit(msg.sender, _amount);
    }

    function withdraw(uint256 _amount) public nonReentrant returns (uint256 currentAmount) {
        UserInfo storage user = userInfo[msg.sender];

        require(_amount > 0, "HunterVault: amount must be greater than zero");
        require(user.stake >= _amount, "HunterVault: withdraw amount exceeds balance");

        if (IS_CAKE_STAKING) {
            STAKED_TOKEN_FARM.leaveStaking(_amount);
        } else {
            STAKED_TOKEN_FARM.withdraw(FARM_PID, _amount);
        }

        currentAmount = _amount;

        if (block.timestamp < user.lastDepositedTime.add(withdrawFeePeriod)) {
            uint256 currentWithdrawFee = currentAmount.mul(earlyWithdrawFee).div(1e4);

            STAKED_TOKEN.safeTransfer(treasury, currentWithdrawFee);

            currentAmount = currentAmount.sub(currentWithdrawFee);

            emit EarlyWithdraw(msg.sender, _amount, currentWithdrawFee);
        }

        user.autoMooseShares = user.autoMooseShares.add(
            user.stake.mul(accSharesPerStakedToken).div(1e18).sub(
                user.rewardDebt
            )
        );
        user.stake = user.stake.sub(_amount);
        user.rewardDebt = user.stake.mul(accSharesPerStakedToken).div(1e18);

        // Withdraw moose rewards if user leaves
        if (user.stake == 0 && user.autoMooseShares > 0) {
            _claimRewards(user.autoMooseShares, false);
        }

        STAKED_TOKEN.safeTransfer(msg.sender, currentAmount);
        emit Withdraw(msg.sender, currentAmount);
    }

    function claimRewards(uint256 _shares) external nonReentrant {
        _claimRewards(_shares, true);
    }

    function _claimRewards(uint256 _shares, bool _update) private {
        UserInfo storage user = userInfo[msg.sender];

        if (_update) {
            user.autoMooseShares = user.autoMooseShares.add(
                user.stake.mul(accSharesPerStakedToken).div(1e18).sub(
                    user.rewardDebt
                )
            );

            user.rewardDebt = user.stake.mul(accSharesPerStakedToken).div(1e18);
        }

        require(user.autoMooseShares >= _shares, "HunterVault: claim amount exceeds balance");

        user.autoMooseShares = user.autoMooseShares.sub(_shares);

        uint256 mooseBalanceBefore = MOOSE.balanceOf(address(this));

        AUTO_MOOSE.withdraw(_shares);

        uint256 withdrawAmount = MOOSE.balanceOf(address(this)).sub(mooseBalanceBefore);

        _safeMOOSETransfer(msg.sender, withdrawAmount);

        emit ClaimRewards(msg.sender, _shares, withdrawAmount);
    }

    function getExpectedOutputs() external view returns (
        uint256 platformOutput,
        uint256 keeperOutput,
        uint256 burnOutput,
        uint256 mooseOutput
    ) {
        uint256 wbnbOutput = _getExpectedOutput(pathToWbnb);
        uint256 mooseOutputWithoutFees = _getExpectedOutput(pathToMoose);

        platformOutput = wbnbOutput.mul(platformFee).div(1e4);
        keeperOutput = wbnbOutput.mul(keeperFee).div(1e4);
        burnOutput = mooseOutputWithoutFees.mul(buyBackRate).div(1e4);

        mooseOutput = mooseOutputWithoutFees.sub(
            mooseOutputWithoutFees.mul(platformFee).div(1e4).add(
                mooseOutputWithoutFees.mul(keeperFee).div(1e4)
            ).add(
                mooseOutputWithoutFees.mul(buyBackRate).div(1e4)
            )
        );
    }

    function _getExpectedOutput(
        address[] memory _path
    ) private view returns (uint256) {
        uint256 rewards = FARM_REWARD_TOKEN.balanceOf(address(this)).add(
            STAKED_TOKEN_FARM.pendingCake(FARM_PID, address(this))
        );

        uint256[] memory amounts = router.getAmountsOut(rewards, _path);

        return amounts[amounts.length.sub(1)];
    }

    function balanceOf(
        address _user
    ) external view returns (
        uint256 stake,
        uint256 moose,
        uint256 autoMooseShares
    ) {
        UserInfo memory user = userInfo[_user];

        uint256 pendingShares = user.stake.mul(accSharesPerStakedToken).div(1e18).sub(
            user.rewardDebt
        );

        stake = user.stake;
        autoMooseShares = user.autoMooseShares.add(pendingShares);
        moose = autoMooseShares.mul(AUTO_MOOSE.getPricePerFullShare()).div(1e18);
    }

    function _approveTokenIfNeeded(
        IERC20Upgradeable _token,
        uint256 _amount,
        address _spender
    ) private {
        if (_token.allowance(address(this), _spender) < _amount) {
            _token.safeIncreaseAllowance(_spender, _amount);
        }
    }

    function totalStake() public view returns (uint256) {
        return STAKED_TOKEN_FARM.userInfo(FARM_PID, address(this));
    }

    function totalAutoMooseShares() public view returns (uint256) {
        (uint256 shares, , ,) = AUTO_MOOSE.userInfo(address(this));

        return shares;
    }
    
    function setPathToMoose(address[] memory _path) external onlyOwner {
        require(
            _path[0] == address(FARM_REWARD_TOKEN) && _path[_path.length - 1] == address(MOOSE),
            "HunterVault: Incorrect path to MOOSE"
        );

        address[] memory oldPath = pathToMoose;

        pathToMoose = _path;

        emit SetPathToMoose(oldPath, pathToMoose);
    }

    function setPathToWbnb(address[] memory _path) external onlyOwner {
        require(
            _path[0] == address(FARM_REWARD_TOKEN) && _path[_path.length - 1] == WBNB,
            "HunterVault: Incorrect path to WBNB"
        );

        address[] memory oldPath = pathToWbnb;

        pathToWbnb = _path;

        emit SetPathToWbnb(oldPath, pathToWbnb);
    }

    function setTreasury(address _treasury) external onlyOwner {
        address oldTreasury = treasury;

        treasury = _treasury;

        emit SetTreasury(oldTreasury, treasury);
    }

    function setKeeper(address _keeper) external onlyOwner {
        address oldKeeper = keeper;

        keeper = _keeper;

        emit SetKeeper(oldKeeper, keeper);
    }

    function setKeeperFee(uint256 _keeperFee) external onlyOwner {
        require(_keeperFee <= keeperFeeUL, "HunterVault: Keeper fee too high");

        uint256 oldKeeperFee = keeperFee;

        keeperFee = _keeperFee;

        emit SetKeeperFee(oldKeeperFee, keeperFee);
    }

    function setPlatform(address _platform) external onlyOwner {
        address oldPlatform = platform;

        platform = _platform;

        emit SetPlatform(oldPlatform, platform);
    }

    function setPlatformFee(uint256 _platformFee) external onlyOwner {
        require(_platformFee <= platformFeeUL, "HunterVault: Platform fee too high");

        uint256 oldPlatformFee = platformFee;

        platformFee = _platformFee;

        emit SetPlatformFee(oldPlatformFee, platformFee);
    }

    function setBuyBackRate(uint256 _buyBackRate) external onlyOwner {
        require(
            _buyBackRate <= buyBackRateUL,
            "HunterVault: Buy back rate too high"
        );

        uint256 oldBuyBackRate = buyBackRate;

        buyBackRate = _buyBackRate;

        emit SetBuyBackRate(oldBuyBackRate, buyBackRate);
    }

    function setEarlyWithdrawFee(uint256 _earlyWithdrawFee) external onlyOwner {
        require(
            _earlyWithdrawFee <= earlyWithdrawFeeUL,
            "HunterVault: Early withdraw fee too high"
        );

        uint256 oldEarlyWithdrawFee = earlyWithdrawFee;

        earlyWithdrawFee = _earlyWithdrawFee;

        emit SetEarlyWithdrawFee(oldEarlyWithdrawFee, earlyWithdrawFee);
    }

    // Safe MOOSE transfer function, just in case if rounding error causes pool to not have enough
    function _safeMOOSETransfer(address _to, uint256 _amount) private {
        uint256 balance = MOOSE.balanceOf(address(this));

        if (_amount > balance) {
            MOOSE.transfer(_to, balance);
        } else {
            MOOSE.transfer(_to, _amount);
        }
    }

    function _swap(
        uint256 _inputAmount,
        uint256 _minOutputAmount,
        address[] memory _path,
        address _to
    ) private returns (uint256[] memory) {
        _approveTokenIfNeeded(
            FARM_REWARD_TOKEN,
            _inputAmount,
            address(router)
        );

        return router.swapExactTokensForTokens(
            _inputAmount,
            _minOutputAmount,
            _path,
            _to,
            block.timestamp
        );
    }

}
