// SPDX-License-Identifier: MIT

pragma solidity 0.8.10;

import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/AddressUpgradeable.sol";

abstract contract MOOSEToken is ERC20 {
    function mint(address _to, uint256 _amount) public virtual;
    function transferOwnership(address newOwner) public virtual;
}

// For interacting with our own strategy
interface IStrategy {
    // Total want tokens managed by strategy
    function wantLockedTotal() external view returns (uint256);

    // Sum of all shares of users to wantLockedTotal
    function sharesTotal() external view returns (uint256);

    // Main want token compounding function
    function earn() external;

    // Transfer want tokens mooseHunt -> strategy
    function deposit(
        address _userAddress,
        uint256 _wantAmt
    ) external returns (uint256);

    // Transfer want tokens strategy -> mooseHunt
    function withdraw(
        address _userAddress,
        uint256 _wantAmt
    ) external returns (uint256);

    function inCaseTokensGetStuck(
        address _token,
        uint256 _amount,
        address _to
    ) external;
}

contract MooseHunt is OwnableUpgradeable, ReentrancyGuardUpgradeable {
    using SafeMathUpgradeable for uint256;
    using SafeERC20Upgradeable for IERC20Upgradeable;

    // Info of each user.
    struct UserInfo {
        uint256 shares; // How many LP tokens the user has provided.
        uint256 rewardDebt; // Reward debt. See explanation below.

        // We do some fancy math here. Basically, any point in time, the amount of MOOSE
        // entitled to a user but is pending to be distributed is:
        //
        //   amount = user.shares / sharesTotal * wantLockedTotal
        //   pending reward = (amount * pool.accMOOSEPerShare) - user.rewardDebt
        //
        // Whenever a user deposits or withdraws want tokens to a pool. Here's what happens:
        //   1. The pool's `accMOOSEPerShare` (and `lastRewardBlock`) gets updated.
        //   2. User receives the pending reward sent to his/her address.
        //   3. User's `amount` gets updated.
        //   4. User's `rewardDebt` gets updated.
    }

    struct PoolInfo {
        IERC20Upgradeable want; // Address of the want token.
        uint256 allocPoint; // How many allocation points assigned to this pool. MOOSE to distribute per block.
        uint256 lastRewardBlock; // Last block number that MOOSE distribution occurs.
        uint256 accMOOSEPerShare; // Accumulated MOOSE per share, times 1e12. See below.
        address strat; // Strategy address that will MOOSE compound want tokens
    }

    address public MOOSE;
    address public burnAddress;

    uint256 public ownerMOOSEReward; // 15% Dev + 5% MKT
    uint256 public maxSupply;
    uint256 public MOOSEPerBlock; // MOOSE tokens created per block
    uint256 public startBlock; // https://bscscan.com/block/countdown/7862758

    PoolInfo[] public poolInfo; // Info of each pool.
    mapping(IERC20Upgradeable => bool) public availableAssets; // Info of each pool.
    mapping(uint256 => mapping(address => UserInfo)) public userInfo; // Info of each user that stakes LP tokens.
    uint256 public totalAllocPoint; // Total allocation points. Must be the sum of all allocation points in all pools.

    event Deposit(address indexed user, uint256 indexed pid, uint256 amount);
    event Withdraw(address indexed user, uint256 indexed pid, uint256 amount);
    event EmergencyWithdraw(
        address indexed user,
        uint256 indexed pid,
        uint256 amount
    );

    function initialize(
        address _moose,
        address _burnAddress,
        uint256 _ownerMOOSEReward,
        uint256 _startBlock
    ) external {
        MOOSE = _moose;
        burnAddress = _burnAddress;
        ownerMOOSEReward = _ownerMOOSEReward;
        startBlock = _startBlock;
        OwnableUpgradeable.__Ownable_init();
    }

    function setAddressConfig(address _moose, address _burnAddress) external onlyOwner {
        MOOSE = _moose;
        burnAddress = _burnAddress;
    }

    function setConfiguration(uint256 _ownerMOOSEReward, uint256 _startBlock, uint256 _totalAllocPoint) external onlyOwner { 
        ownerMOOSEReward = _ownerMOOSEReward;
        startBlock = _startBlock;
        totalAllocPoint = _totalAllocPoint;       
    }

    function poolLength() external view returns (uint256) {
        return poolInfo.length;
    }

    // Return reward multiplier over the given _from to _to block.
    function getMultiplier(uint256 _from, uint256 _to) public view returns (uint256) {
        if (IERC20Upgradeable(MOOSE).totalSupply() >= maxSupply) {
            return 0;
        }
        return _to.sub(_from);
    }

    // View function to see pending MOOSE on frontend.
    function pendingMOOSE(uint256 _pid, address _user) external view returns (uint256) {
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][_user];
        uint256 accMOOSEPerShare = pool.accMOOSEPerShare;
        uint256 sharesTotal = IStrategy(pool.strat).sharesTotal();
        if (block.number > pool.lastRewardBlock && sharesTotal != 0) {
            uint256 multiplier = getMultiplier(pool.lastRewardBlock, block.number);
            uint256 MOOSEReward = multiplier.mul(MOOSEPerBlock).mul(pool.allocPoint).div(
                totalAllocPoint
            );
            accMOOSEPerShare = accMOOSEPerShare.add(
                MOOSEReward.mul(1e12).div(sharesTotal)
            );
        }
        return user.shares.mul(accMOOSEPerShare).div(1e12).sub(user.rewardDebt);
    }

    // View function to see staked Want tokens on frontend.
    function stakedWantTokens(uint256 _pid, address _user) external view returns (uint256) {
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][_user];

        uint256 sharesTotal = IStrategy(pool.strat).sharesTotal();
        uint256 wantLockedTotal = IStrategy(poolInfo[_pid].strat).wantLockedTotal();
        if (sharesTotal == 0) {
            return 0;
        }
        return user.shares.mul(wantLockedTotal).div(sharesTotal);
    }

    // Update reward variables for all pools. Be careful of gas spending!
    function massUpdatePools() public {
        uint256 length = poolInfo.length;
        for (uint256 pid = 0; pid < length; ++pid) {
            updatePool(pid);
        }
    }

    // Update reward variables of the given pool to be up-to-date.
    function updatePool(uint256 _pid) public {
        PoolInfo storage pool = poolInfo[_pid];
        if (block.number <= pool.lastRewardBlock) {
            return;
        }
        uint256 sharesTotal = IStrategy(pool.strat).sharesTotal();
        if (sharesTotal == 0) {
            pool.lastRewardBlock = block.number;
            return;
        }
        uint256 multiplier = getMultiplier(pool.lastRewardBlock, block.number);
        if (multiplier <= 0) {
            return;
        }
        uint256 MOOSEReward = multiplier.mul(MOOSEPerBlock).mul(pool.allocPoint).div(
            totalAllocPoint
        );

        MOOSEToken(MOOSE).mint(
            owner(),
            MOOSEReward.mul(ownerMOOSEReward).div(1000)
        );
        MOOSEToken(MOOSE).mint(address(this), MOOSEReward);

        pool.accMOOSEPerShare = pool.accMOOSEPerShare.add(
            MOOSEReward.mul(1e12).div(sharesTotal)
        );
        pool.lastRewardBlock = block.number;
    }

    // Want tokens moved from user -> MOOSEHunt (MOOSE allocation) -> Strat (compounding)
    function deposit(uint256 _pid, uint256 _wantAmt) external nonReentrant {
        updatePool(_pid);
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][msg.sender];

        if (user.shares > 0) {
            uint256 pending = user.shares.mul(pool.accMOOSEPerShare).div(1e12).sub(
                user.rewardDebt
            );
            if (pending > 0) {
                safeMOOSETransfer(msg.sender, pending);
            }
        }
        if (_wantAmt > 0) {
            pool.want.safeTransferFrom(
                address(msg.sender),
                address(this),
                _wantAmt
            );

            pool.want.safeIncreaseAllowance(pool.strat, _wantAmt);
            uint256 sharesAdded = IStrategy(poolInfo[_pid].strat).deposit(msg.sender, _wantAmt);
            user.shares = user.shares.add(sharesAdded);
        }
        user.rewardDebt = user.shares.mul(pool.accMOOSEPerShare).div(1e12);
        emit Deposit(msg.sender, _pid, _wantAmt);
    }

    // Withdraw LP tokens from MasterChef.
    function withdraw(uint256 _pid, uint256 _wantAmt) public nonReentrant {
        updatePool(_pid);

        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][msg.sender];

        uint256 wantLockedTotal = IStrategy(poolInfo[_pid].strat).wantLockedTotal();
        uint256 sharesTotal = IStrategy(poolInfo[_pid].strat).sharesTotal();

        require(user.shares > 0, "user.shares is 0");
        require(sharesTotal > 0, "sharesTotal is 0");

        // Withdraw pending MOOSE
        uint256 pending = user.shares.mul(pool.accMOOSEPerShare).div(1e12).sub(
            user.rewardDebt
        );
        if (pending > 0) {
            safeMOOSETransfer(msg.sender, pending);
        }

        // Withdraw want tokens
        uint256 amount = user.shares.mul(wantLockedTotal).div(sharesTotal);
        if (_wantAmt > amount) {
            _wantAmt = amount;
        }
        if (_wantAmt > 0) {
            uint256 sharesRemoved = IStrategy(poolInfo[_pid].strat).withdraw(msg.sender, _wantAmt);

            if (sharesRemoved > user.shares) {
                user.shares = 0;
            } else {
                user.shares = user.shares.sub(sharesRemoved);
            }

            uint256 wantBal = IERC20Upgradeable(pool.want).balanceOf(address(this));
            if (wantBal < _wantAmt) {
                _wantAmt = wantBal;
            }
            pool.want.safeTransfer(address(msg.sender), _wantAmt);
        }
        user.rewardDebt = user.shares.mul(pool.accMOOSEPerShare).div(1e12);
        emit Withdraw(msg.sender, _pid, _wantAmt);
    }

    function withdrawAll(uint256 _pid) external nonReentrant {
        withdraw(_pid, 2**256 - 1);
    }

    // Withdraw without caring about rewards. EMERGENCY ONLY.
    function emergencyWithdraw(uint256 _pid) external nonReentrant {
        PoolInfo storage pool = poolInfo[_pid];
        UserInfo storage user = userInfo[_pid][msg.sender];

        uint256 wantLockedTotal =
        IStrategy(poolInfo[_pid].strat).wantLockedTotal();
        uint256 sharesTotal = IStrategy(poolInfo[_pid].strat).sharesTotal();
        uint256 amount = user.shares.mul(wantLockedTotal).div(sharesTotal);

        IStrategy(poolInfo[_pid].strat).withdraw(msg.sender, amount);

        pool.want.safeTransfer(address(msg.sender), amount);
        emit EmergencyWithdraw(msg.sender, _pid, amount);
        user.shares = 0;
        user.rewardDebt = 0;
    }

    // Safe MOOSE transfer function, just in case if rounding error causes pool to not have enough
    function safeMOOSETransfer(address _to, uint256 _MOOSEAmt) internal {
        uint256 MOOSEBal = IERC20Upgradeable(MOOSE).balanceOf(address(this));
        if (_MOOSEAmt > MOOSEBal) {
            IERC20Upgradeable(MOOSE).transfer(_to, MOOSEBal);
        } else {
            IERC20Upgradeable(MOOSE).transfer(_to, _MOOSEAmt);
        }
    }

    /*
        ------------------------------------
                Governance functions
        ------------------------------------
    */

    // Add a new lp to the pool. Can only be called by the owner.
    // XXX DO NOT add the same LP token more than once. Rewards will be messed up if you do. (Only if want tokens are stored here.)
    function addPool(
        uint256 _allocPoint,
        IERC20Upgradeable _want,
        bool _withUpdate,
        address _strat
    ) external onlyOwner {
        require(!availableAssets[_want], "Can't add another pool of same asset");
        if (_withUpdate) {
            massUpdatePools();
        }
        uint256 lastRewardBlock = block.number > startBlock ? block.number : startBlock;
        totalAllocPoint = totalAllocPoint.add(_allocPoint);
        poolInfo.push(
            PoolInfo({
        want: _want,
        allocPoint: _allocPoint,
        lastRewardBlock: lastRewardBlock,
        accMOOSEPerShare: 0,
        strat: _strat
        })
        );
        availableAssets[_want] = true;
    }

    // Update the given pool's MOOSE allocation point. Can only be called by the owner.
    function set(
        uint256 _pid,
        uint256 _allocPoint,
        bool _withUpdate
    ) external onlyOwner {
        if (_withUpdate) {
            massUpdatePools();
        }
        totalAllocPoint = totalAllocPoint.sub(poolInfo[_pid].allocPoint).add(
            _allocPoint
        );
        poolInfo[_pid].allocPoint = _allocPoint;
    }

    function setMaxSupply(uint256 _maxSupply) public onlyOwner {
        maxSupply = _maxSupply;
    }

    function setMoosePerBlock(uint256 _MOOSEPerBlock) public onlyOwner {
        MOOSEPerBlock = _MOOSEPerBlock;
    }

    function inCaseTokensGetStuck(address _token, uint256 _amount) external onlyOwner {
        require(_token != MOOSE, "!safe");
        IERC20Upgradeable(_token).safeTransfer(msg.sender, _amount);
    }

    // for emergency, deploy new contract
    function transferMOOSEOwner(address _newOwner) external onlyOwner {
        MOOSEToken(MOOSE).transferOwnership(_newOwner);
    }
}