// SPDX-License-Identifier: MIT

pragma solidity 0.8.10;

import "@openzeppelin/contracts-upgradeable/security/ReentrancyGuardUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/security/PausableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/access/OwnableUpgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/IERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/token/ERC20/utils/SafeERC20Upgradeable.sol";
import "@openzeppelin/contracts-upgradeable/utils/math/SafeMathUpgradeable.sol";

abstract contract Governable {
    address public govAddress;

    event SetGov(address _govAddress);

    modifier onlyAllowGov() {
        require(msg.sender == govAddress, "!gov");
        _;
    }

    function setGov(address _govAddress) public virtual onlyAllowGov {
        govAddress = _govAddress;
        emit SetGov(_govAddress);
    }
}

// moose single token farm strategy
contract StratMoose is OwnableUpgradeable, ReentrancyGuardUpgradeable, PausableUpgradeable, Governable {
    using SafeMathUpgradeable for uint256;
    using SafeERC20Upgradeable for IERC20Upgradeable;

    address public wantAddress;
    address public mooseFarmAddress;

    // Total want tokens managed by strategy
    uint256 public wantLockedTotal;

    // Sum of all shares of users to wantLockedTotal
    uint256 public sharesTotal;

    uint256 public entranceFeeFactor; // No deposit fees
    uint256 public entranceFeeFactorMax;
    uint256 public entranceFeeFactorLL; // 0.5% is the max entrance fee settable. LL = lowerlimit

    uint256 public withdrawFeeFactor; // No withdraw fees
    uint256 public withdrawFeeFactorMax;
    uint256 public withdrawFeeFactorLL; // 0.5% is the max entrance fee settable. LL = lowerlimit

    event SetSettings(uint256 _entranceFeeFactor, uint256 _withdrawFeeFactor);

    function initialize(address _moose, address _mooseFarmAddress, address _govAddress) external {
        wantAddress = _moose;
        mooseFarmAddress = _mooseFarmAddress;
        govAddress = _govAddress;

        OwnableUpgradeable.__Ownable_init();
        transferOwnership(_mooseFarmAddress);
    }

    function setConfig(
        uint256 _entranceFeeFactor,
        uint256 _entranceFeeFactorMax,
        uint256 _entranceFeeFactorLL,
        uint256 _withdrawFeeFactor,
        uint256 _withdrawFeeFactorMax,
        uint256 _withdrawFeeFactorLL
    ) external onlyAllowGov {
        entranceFeeFactor = _entranceFeeFactor;
        entranceFeeFactorMax = _entranceFeeFactorMax;
        entranceFeeFactorLL = _entranceFeeFactorLL;

        withdrawFeeFactor = _withdrawFeeFactor;
        withdrawFeeFactorMax = _withdrawFeeFactorMax;
        withdrawFeeFactorLL = _withdrawFeeFactorLL;
    }

    // This contract doesn't auto-compound
    // earn() function is here to follow the Strategy interface
    function earn() external {}

    // Transfer want tokens mooseFarm -> strategy
    function deposit(        
        address _userAddress,
        uint256 _wantAmt
    ) external virtual onlyOwner nonReentrant whenNotPaused returns (uint256) {
        IERC20Upgradeable(wantAddress).safeTransferFrom(
            address(msg.sender),
            address(this),
            _wantAmt
        );

        uint256 sharesAdded = _wantAmt;

        if (wantLockedTotal > 0 && sharesTotal > 0) {
            sharesAdded = _wantAmt
            .mul(sharesTotal)
            .mul(entranceFeeFactor)
            .div(wantLockedTotal)
            .div(entranceFeeFactorMax);
        }

        sharesTotal = sharesTotal.add(sharesAdded);

        wantLockedTotal = wantLockedTotal.add(_wantAmt);

        return sharesAdded;
    }

    // Transfer want tokens strategy -> mooseFarm
    function withdraw(
        address _userAddress,
        uint256 _wantAmt
    ) external virtual onlyOwner nonReentrant returns (uint256) {
        require(_wantAmt > 0, "_wantAmt <= 0");

        uint256 sharesRemoved = _wantAmt.mul(sharesTotal).div(wantLockedTotal);

        if (sharesRemoved > sharesTotal) {
            sharesRemoved = sharesTotal;
        }

        sharesTotal = sharesTotal.sub(sharesRemoved);

        if (withdrawFeeFactor < withdrawFeeFactorMax) {
            _wantAmt = _wantAmt.mul(withdrawFeeFactor).div(
                withdrawFeeFactorMax
            );
        }

        uint256 wantAmt = IERC20Upgradeable(wantAddress).balanceOf(address(this));

        if (_wantAmt > wantAmt) {
            _wantAmt = wantAmt;
        }

        if (wantLockedTotal < _wantAmt) {
            _wantAmt = wantLockedTotal;
        }

        wantLockedTotal = wantLockedTotal.sub(_wantAmt);

        IERC20Upgradeable(wantAddress).safeTransfer(mooseFarmAddress, _wantAmt);

        return sharesRemoved;
    }

    function inCaseTokensGetStuck(
        address _token,
        uint256 _amount,
        address _to
    ) external virtual onlyAllowGov {
        require(_token != wantAddress, "!safe");
        IERC20Upgradeable(_token).safeTransfer(_to, _amount);
    }

    function setSettings(
        uint256 _entranceFeeFactor,
        uint256 _withdrawFeeFactor
    ) external virtual onlyAllowGov {
        require(
            _entranceFeeFactor >= entranceFeeFactorLL,
            "_entranceFeeFactor too low"
        );
        require(
            _entranceFeeFactor <= entranceFeeFactorMax,
            "_entranceFeeFactor too high"
        );
        entranceFeeFactor = _entranceFeeFactor;

        require(
            _withdrawFeeFactor >= withdrawFeeFactorLL,
            "_withdrawFeeFactor too low"
        );
        require(
            _withdrawFeeFactor <= withdrawFeeFactorMax,
            "_withdrawFeeFactor too high"
        );
        withdrawFeeFactor = _withdrawFeeFactor;

        emit SetSettings(_entranceFeeFactor, _withdrawFeeFactor);
    }
}